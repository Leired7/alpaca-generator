import { shallowMount } from '@vue/test-utils';
import AlpacaGenerator from '@/components/AlpacaGenerator.vue';

describe('AlpacaGenerator', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(AlpacaGenerator, {
      propsData: { title: 'Alpaca Generator' },
    });
  });

  afterAll(() => wrapper.destroy());

  it('should be mounted', () => {
    expect(wrapper).toBeTruthy();
  });

  it('Displays title', () => {
    expect(wrapper.find('[data-testid="title"]').text()).toMatch(
      'Alpaca Generator'
    );
  });

  describe('Display alpaca image', () => {
    it('Displays the image tag for alpaca', () => {
      expect(wrapper.findAll('img').length).toBe(9);
    });

    it('Displays the background image for alpaca', () => {
      expect(wrapper.find('[data-tesitd="alpaca-wrapper"]')).toBeTruthy();
      expect(
        wrapper.find('[data-testid="backgrounds-image-blue50"]').attributes()
          .alt
      ).toBe('Alpaca blue50 backgrounds');
    });

    it('Displays the alpaca neck', () => {
      expect(
        wrapper.find('[data-testid="neck-image-default"]').attributes().alt
      ).toBe('Alpaca default neck');
    });

    it('Displays the alpaca nose', () => {
      expect(
        wrapper.find('[data-testid="nose-image-nose"]').attributes().alt
      ).toBe('Alpaca nose nose');
    });

    it('Displays the alpaca eyes', () => {
      expect(
        wrapper.find('[data-testid="eyes-image-default"]').attributes().alt
      ).toBe('Alpaca default eyes');
    });

    it('Displays the alpaca ears', () => {
      expect(
        wrapper.find('[data-testid="ears-image-default"]').attributes().alt
      ).toBe('Alpaca default ears');
    });

    it('Displays the alpaca hair', () => {
      expect(
        wrapper.find('[data-testid="hair-image-default"]').attributes().alt
      ).toBe('Alpaca default hair');
    });

    it('Displays the alpaca leg', () => {
      expect(
        wrapper.find('[data-testid="leg-image-default"]').attributes().alt
      ).toBe('Alpaca default leg');
    });

    it('Displays the alpaca mouth', () => {
      expect(
        wrapper.find('[data-testid="mouth-image-default"]').attributes().alt
      ).toBe('Alpaca default mouth');
    });

    it('Displays the alpaca accesories', () => {
      expect(
        wrapper.find('[data-testid="accessories-image-flower"]').attributes()
          .alt
      ).toBe('Alpaca flower accessories');
    });
  });

  describe('Display "Accesorize the Alpaca\'s panel"', () => {
    it('Displays the panel title', () => {
      expect(wrapper.find('[data-testid="accesorize-title"]').text()).toMatch(
        "Accesorize the Alpaca's"
      );
    });

    it('Display all accesorize panel buttons', () => {
      expect(
        wrapper.findAll('[data-testid^="accesorize-button-"]').length
      ).toBe(9);
    });

    it('Display hair button', () => {
      expect(
        wrapper.find('[data-testid="accesorize-button-hair"]').text()
      ).toMatch('hair');
    });

    it('On hair button click must show hair options', async () => {
      const hairButton = wrapper.find('[data-testid="accesorize-button-hair"]');
      const stylePanelOptions = wrapper.find(
        '[data-testid="style-panel-options"]'
      );

      await hairButton.trigger('click');

      expect(stylePanelOptions.findAll('button').length).toBe(7);
    });

    it('Display ears button', () => {
      expect(
        wrapper.find('[data-testid="accesorize-button-ears"]').text()
      ).toMatch('ears');
    });

    it('On ears button click must show ears options', async () => {
      const earsButton = wrapper.find('[data-testid="accesorize-button-ears"]');
      const stylePanelOptions = wrapper.find(
        '[data-testid="style-panel-options"]'
      );

      await earsButton.trigger('click');

      expect(stylePanelOptions.findAll('button').length).toBe(3);
    });

    it('Display eyes button', () => {
      expect(
        wrapper.find('[data-testid="accesorize-button-eyes"]').text()
      ).toMatch('eyes');
    });

    it('On eyes button click must show eyes options', async () => {
      const eyesButton = wrapper.find('[data-testid="accesorize-button-eyes"]');
      const stylePanelOptions = wrapper.find(
        '[data-testid="style-panel-options"]'
      );

      await eyesButton.trigger('click');

      expect(stylePanelOptions.findAll('button').length).toBe(6);
    });

    it('Display mouth button', () => {
      expect(
        wrapper.find('[data-testid="accesorize-button-mouth"]').text()
      ).toMatch('mouth');
    });

    it('On mouth button click must show mouth options', async () => {
      const mouthButton = wrapper.find(
        '[data-testid="accesorize-button-mouth"]'
      );
      const stylePanelOptions = wrapper.find(
        '[data-testid="style-panel-options"]'
      );

      await mouthButton.trigger('click');

      expect(stylePanelOptions.findAll('button').length).toBe(5);
    });

    it('Display neck button', () => {
      expect(
        wrapper.find('[data-testid="accesorize-button-neck"]').text()
      ).toMatch('neck');
    });

    it('On neck button click must show neck options', async () => {
      const neckButton = wrapper.find('[data-testid="accesorize-button-neck"]');
      const stylePanelOptions = wrapper.find(
        '[data-testid="style-panel-options"]'
      );

      await neckButton.trigger('click');

      expect(stylePanelOptions.findAll('button').length).toBe(4);
    });

    it('Display leg button', () => {
      expect(
        wrapper.find('[data-testid="accesorize-button-leg"]').text()
      ).toMatch('leg');
    });

    it('On leg button click must show leg options', async () => {
      const legButton = wrapper.find('[data-testid="accesorize-button-leg"]');
      const stylePanelOptions = wrapper.find(
        '[data-testid="style-panel-options"]'
      );

      await legButton.trigger('click');

      expect(stylePanelOptions.findAll('button').length).toBe(6);
    });

    it('Display accesories button', () => {
      expect(
        wrapper.find('[data-testid="accesorize-button-accesories"]').text()
      ).toMatch('accesories');
    });

    it('On accesories button click must show accesories options', async () => {
      const accesoriesButton = wrapper.find(
        '[data-testid="accesorize-button-accesories"]'
      );
      const stylePanelOptions = wrapper.find(
        '[data-testid="style-panel-options"]'
      );

      await accesoriesButton.trigger('click');

      expect(stylePanelOptions.findAll('button').length).toBe(4);
    });

    it('Display background button', () => {
      expect(
        wrapper.find('[data-testid="accesorize-button-backgrounds"]').text()
      ).toMatch('backgrounds');
    });

    it('On backgrounds button click must show backgrounds options', async () => {
      const backgroundsButton = wrapper.find(
        '[data-testid="accesorize-button-backgrounds"]'
      );
      const stylePanelOptions = wrapper.find(
        '[data-testid="style-panel-options"]'
      );

      await backgroundsButton.trigger('click');

      expect(stylePanelOptions.findAll('button').length).toBe(18);
    });

    it('Should have active class after click in', async () => {
      const hairButton = wrapper.find('[data-testid="accesorize-button-hair"]');

      await hairButton.trigger('click');

      expect(hairButton.find('.active')).toBeTruthy();
    });
  });

  describe('Display "Style" panel', () => {
    it('Should exits', () => {
      expect(wrapper.find('[data-testid="style-panel-options"]')).toBeTruthy();
    });

    it('Button has been called', async () => {
      const hairButton = wrapper.find('[data-testid="accesorize-button-hair"]');

      hairButton.trigger('click');

      await wrapper.vm.$nextTick();

      expect(wrapper.emitted().click).toBeTruthy();
    });

    it('Display "Click in any button to show its options" title', () => {
      expect(wrapper.find('[data-testid="style-panel-title"]').text()).toMatch(
        'Click in any button to show its options'
      );
    });

    it('Change the hair style to "curls" when click "curls" button', async () => {
      const hairButton = wrapper.find('[data-testid="accesorize-button-hair"]');

      await hairButton.trigger('click');

      const curlsButton = wrapper.find('[data-testid="button-curls"]');

      await curlsButton.trigger('click');

      expect(
        wrapper.find('[data-testid="hair-image-curls"]').attributes().alt
      ).toMatch('Alpaca curls hair');
    });

    it('Should have active class after click in', async () => {
      const hairButton = wrapper.find('[data-testid="accesorize-button-hair"]');

      await hairButton.trigger('click');

      const curlsButton = wrapper.find('[data-testid="button-curls"]');

      await curlsButton.trigger('click');

      expect(curlsButton.find('.active')).toBeTruthy();
    });
  });

  describe('"Random Images" button', () => {
    it('should exits and contain "Random Images" text', () => {
      const randomButton = wrapper.find('[data-testid="random-button"]');

      expect(randomButton.text()).toContain('Random Images');
    });

    it('triggers a click and call "randomImages" method', async () => {
      const mockMethod = jest.spyOn(wrapper.vm, 'randomImages');
      const randomButton = wrapper.find('[data-testid="random-button"]');

      await randomButton.trigger('click');

      expect(mockMethod).toHaveBeenCalledTimes(1);
    });

    it('should change randomly all alapaca body parts and background color images', async () => {
      const initAlpacaBackgroundImage = wrapper.vm.alpacaImage[0].options;

      const randomButton = wrapper.find('[data-testid="random-button"]');

      await randomButton.trigger('click');

      const newAlpacaBackgroundImage = wrapper.vm.alpacaImage[0].options;

      expect(initAlpacaBackgroundImage).not.toBe(newAlpacaBackgroundImage);
    });
  });

  describe('"Download Image" button', () => {
    it('should exits and contain "Random Images" text', () => {
      const randomButton = wrapper.find('[data-testid="download-button"]');

      expect(randomButton.text()).toContain('Download Image');
    });

    it('triggers a click and call "downloadImage" method', async () => {
      const mockMethod = jest.spyOn(wrapper.vm, 'downloadImage');
      const randomButton = wrapper.find('[data-testid="download-button"]');

      await randomButton.trigger('click');

      expect(mockMethod).toHaveBeenCalledTimes(1);
    });
  });
});
